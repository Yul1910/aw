﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using PlayerWebApp.Models;

namespace PlayerWebApp.Controllers
{
    public class TrackModelController : Controller
    {
        private readonly TrackContext _context;

        public TrackModelController(TrackContext context)
        {
            _context = context;
        }
        // GET: TrackModel
        public ActionResult Index()
        {
            return View();
        }

        // GET: TrackModel/Details/5
        public ActionResult Details(int id)
        {
            return View();
        }

        // GET: TrackModel/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: TrackModel/Create
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(IFormCollection collection)
        {
            try
            {
                // TODO: Add insert logic here

                return RedirectToAction(nameof(Index));
            }
            catch
            {
                return View();
            }
        }

        // GET: TrackModel/Edit/5
        public ActionResult Edit(int id)
        {
            return View();
        }

        // POST: TrackModel/Edit/5
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(int id, IFormCollection collection)
        {
            try
            {
                // TODO: Add update logic here

                return RedirectToAction(nameof(Index));
            }
            catch
            {
                return View();
            }
        }

        // GET: TrackModel/Delete/5
        public ActionResult Delete(int id)
        {
            return View();
        }

        // POST: TrackModel/Delete/5
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Delete(int id, IFormCollection collection)
        {
            try
            {
                // TODO: Add delete logic here

                return RedirectToAction(nameof(Index));
            }
            catch
            {
                return View();
            }
        }
    }
}