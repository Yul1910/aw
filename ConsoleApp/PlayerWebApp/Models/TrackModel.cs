﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PlayerWebApp.Models
{
    public class TrackModel
    {
        public int TrackId { get; set; }
        public string Name { get; set; }
        public string TrackUrl { get; set; }
    }
}
