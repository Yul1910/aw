﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;


namespace PlayerWebApp.Models
{
    public class TrackContext : DbContext
    {   public TrackContext()
            :base()
        {

        }
        public DbSet<TrackModel> Tracks { get; set; }
    }
}
