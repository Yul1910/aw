﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using PlayerWebApp.Models;


namespace PlayerWebApp
{
    public class NinjectDependencyResolver: IDependencyResolver
    {
        private IKernel kernel;
        public NinjectDependencyResolver (IKernel kernelParam)
        {
            kernel = kernelParam;
            AddBingdings();
        }
        public object GetService(Type serviceType)
        {
            return kernel.TryGet(serviceType);
        }
        public IEnumerable<object> GetServices(Type serviceType)
        {
            return kernel.GetAll(serviceType);
        }
        private void AddBingdings()
        {
            kernel.Bind<TrackContext>().ToSelf().InTransientScope();

        }

    }
}
