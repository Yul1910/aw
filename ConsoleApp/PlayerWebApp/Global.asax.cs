﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using System.Web.Optimization;
using System.Web.Routing;

namespace PlayerWebApp
{
    public class MvcApplication:System.Web.HttpApplication
    {
        private static IKernel container = new StandardKernal();
        protected void Application_Start()
        {
            AreaRegistrarion.RegisterAllAreas();
            FilterConfig.RegisterGlobalFilters(GlobalFilters.Filters);
            RouteConfig.RegisterRoutes(RouteTable.Routes);
            BundleConfig.RegisterBundles(BundleTable.Bundles);

            System.Web.Mvc.DependencyResolver.SetResolver(new NinjectDependencyResolver(container));
        }

    }
}
