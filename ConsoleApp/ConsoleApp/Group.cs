﻿using System;
using System.Collections.Generic;
using System.Text;
using Microsoft.EntityFrameworkCore;

namespace ConsoleApp
{
    public class Group
    {
        public int GroupId { get; set; }
        public string Name { get; set; }
        public string DateOfCreation { get; set; }
        public int NumberOfMembers { get; set; }
        public virtual ICollection<Track> Tracks { get; set; }
        public virtual ICollection<Album> Albums { get; set; }

    }
}
