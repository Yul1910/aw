﻿using System;
using Microsoft.EntityFrameworkCore;



namespace ConsoleApp
{
   class Program
    {
        private static void SaveTrack()
        { using (PlayerApp db = new PlayerApp())
            {
                db.Add(new Track()
                {
                    TrackId = 1,
                    Name = "Thunder",
                    Genre = new Genre()
                    {
                        GenreId = 1,
                        Name = "Поп-рок"
                    },
                    Album = new Album()
                    {
                        AlbumId = 1,
                        Name = "Evolve",
                        DateOfRelease = "23.06.2017",
                        Group = new Group()
                        {
                            GroupId = 1,
                            Name="Imagine Dragons",
                            DateOfCreation="2008 г.",
                            NumberOfMembers=4
                        }
                    },
                    Group = new Group()
                    {
                        GroupId = 1,
                        Name = "Imagine Dragons",
                        DateOfCreation = "2008 г.",
                        NumberOfMembers = 4
                    }



                });
             
                db.Add(new Track()
                {
                    TrackId = 2,
                    Name = "Machine",
                    Genre = new Genre()
                    {
                        GenreId = 1,
                        Name = "Поп-рок"
                    },
                    Album = new Album()
                    {
                        AlbumId = 2,
                        Name = "Origins",
                        DateOfRelease = "09.11.2018",
                        Group = new Group()
                        {
                            GroupId = 1,
                            Name = "Imagine Dragons",
                            DateOfCreation = "2008 г.",
                            NumberOfMembers = 4
                        }
                    },
                    Group = new Group()
                    {
                        GroupId = 1,
                        Name = "Imagine Dragons",
                        DateOfCreation = "2008 г.",
                        NumberOfMembers = 4
                    }
                });
                db.SaveChanges();
            }
            

        }
        
            public static void Main(string[] args)
            {
                SaveTrack();
                ReadTrack();

                Console.ReadLine();
            }
       
        private static void ReadTrack()
        {
            using (PlayerApp db = new PlayerApp())
            {
                foreach (Track track in db.Tracks)
                {
                    Console.WriteLine(track.Name);
                }

            }


        }
    }
  

}
