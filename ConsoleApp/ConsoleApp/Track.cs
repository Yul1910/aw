﻿using System;
using System.Collections.Generic;
using System.Text;
using Microsoft.EntityFrameworkCore;

namespace ConsoleApp
{
    public class Track
    {
        public int TrackId { get; set;}
        public string Name { get; set; }
        public virtual Genre Genre { get; set; }
        public virtual Album Album { get; set; }
        public virtual Group Group { get; set; }

    }
}
