﻿using System;
using Microsoft.EntityFrameworkCore;



namespace ConsoleApp
{
    public class PlayerApp:DbContext
    {
        public DbSet<Track> Tracks { get; set; }
        public DbSet<Album> Albums { get; set; }
        public DbSet<Genre> Genres { get; set; }
        public DbSet<Group> Groups { get; set; }
        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            optionsBuilder.UseSqlite("Data Source=TrackDb.db");
        }
       
    }
  

}
