﻿using System;
using System.Collections.Generic;
using System.Text;
using Microsoft.EntityFrameworkCore;

namespace ConsoleApp
{
    public class Album
    {
        public int AlbumId { get; set; }
        public string Name { get; set; }
        public string DateOfRelease { get; set; }
        public virtual Group Group { get; set; }
        public virtual ICollection<Track> Tracks { get; set; }
    }
}
